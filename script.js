const board = document.getElementById('board');
const welcomeBox = document.getElementById('welcome-box');
const avatar1 = document.getElementById('avatar1');
const avatar2 = document.getElementById('avatar2');
const turnImage1 = document.getElementById('turn1');
const turnImage2 = document.getElementById('turn2');
const playerCall1 = document.querySelector('#avatar1 div p');
const playerCall2 = document.querySelector('#avatar2 div p');
const playButton = document.getElementById('play');
let column;
let holes;
let currentPlayer = 1;
let playerSlots = [
[0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0],
[0, 0, 0, 0, 0, 0, 0]
];
let player1Name;
let player2Name;
let playerSlotsRow;
let playerSlotsColumn;
const resultBox = document.getElementById('result-box');
const resultImage = resultBox.querySelector('img');
const resultMessage = document.getElementById('message');
const resetButton = document.getElementById('reset');


const sayWelcome = () => {
  welcomeBox.classList.add('active');
}

const setPlayers = () => {
  player1Name = document.getElementById('player1').value || 'Player 1';
  player2Name = document.getElementById('player2').value || 'Player 2';
}

const closeWelcome = () => {
  playButton.addEventListener('click', (event) => {
    setPlayers();
    welcomeBox.classList.remove('active');
    avatar1.classList.add('show');
    playerCall1.textContent = `Your turn ${player1Name}`;
    avatar2.classList.add('show');
    playerCall2.textContent = `Your turn ${player2Name}`;
  });
}

const renderBoard = () => {
  for (let i = 1; i <= 7; i++) {
    column = document.createElement('div');
    column.setAttribute('class', 'boardColumn');
    column.setAttribute('data-columnId', `${i}`);

    for (let j = 6; j >= 1; j--) {
      let row = document.createElement('div');
      row.setAttribute('class', 'hole');
      row.setAttribute('data-id', `${i}, ${j}`);

      column.appendChild(row);
      // console.log(row.dataset.id); 
    }

    board.appendChild(column);
  }
}

const startGame = () => {
  columnList = document.querySelectorAll('.boardColumn');

  columnList.forEach(hole => hole.addEventListener('click', (event) => {
    let currentColumn = event.target.parentNode;

    if (currentColumn.classList.contains('boardColumn')) {
      addBall(currentPlayer, currentColumn);
    }
    
    // addballtoarray(currentPlayer, currentColumn);
  }));
}

const changePlayer = () => {
  if (currentPlayer === 1) {
    currentPlayer = 2;
    playerCall2.classList.add('turn');
    playerCall1.classList.remove('turn');
    turnImage1.classList.remove('turn');
    turnImage2.classList.add('turn');
  } else {
    currentPlayer = 1;
    playerCall1.classList.add('turn');
    playerCall2.classList.remove('turn');
    turnImage2.classList.remove('turn');
    turnImage1.classList.add('turn');
  }
}

const addBall = (player, currentColumn) => {
  holes = currentColumn.childNodes;

  let freeHole;
 
  for (let i = 0; i < 6; i++) {
    let currentHole = holes[i];
  
    if (!currentHole.classList.contains('blocked')) {
      playerSlotsColumn = currentHole.dataset.id[0] - 1;
      playerSlotsRow = currentHole.dataset.id[3] - 1;
      playerSlots[playerSlotsRow][playerSlotsColumn] = player;
      freeHole = currentHole;
      break;
    }
  }

   let result = checkVictory(playerSlots);
   let isDraw = empate();

  if (result) {
    freeHole.classList.add(`player${player}`);
    showResult(result);
    return;
  } else if (isDraw) {
    freeHole.classList.add(`player${player}`);
    showResult(isDraw);
    return;
  }

  var reverse_playerSlots = playerSlots.slice().reverse();

  result = checkVictory(reverse_playerSlots);

  if (result) {
    freeHole.classList.add(`player${player}`);
    showResult(result);
    return;
  } else if (isDraw) {
    freeHole.classList.add(`player${player}`);
    showResult(isDraw);
    return;
  }


  freeHole.classList.add(`player${player}`);
  freeHole.classList.add('blocked');
  changePlayer();
}

const reset__game = () => {
  
  window.location.reload();
}

const empate = () => {
  let isDraw = true;

  for (let i = 0; i < 7; i++) {
    if(playerSlots[0][i] === 0){
      isDraw = false;
    }
  }

  return isDraw;
}

const checkVictory = (player) => {
  const edgeX = player[0].length - 3;
  const edgeY = player.length - 3;

  for (let row = 0; row < player.length; row++) {
    for (let column = 0; column < edgeX; column++) {
      let slot = player[row][column];

      if (slot !== 0) {
        if (slot === player[row][column + 1] && slot === player[row][column + 2] && slot === player[row][column + 3]) {
          return currentPlayer;
        }
      }
    }
  }


  for (let row = 0; row < edgeY; row++) {
    for (let column = 0; column < player[0].length; column++) {
      slot = player[row][column];

      if (slot !== 0) {
        if (slot === player[row + 1][column] && slot === player[row + 2][column] && slot === player[row + 3][column]) {
          return currentPlayer;
        }
      }
    }
  }

  for (let row = 0; row < edgeY; row++) {
    for (let column = 0; column < player[0].length; column++) {
      slot = player[row][column];
      
      if (slot !== 0) {
        if (slot === player[row + 1][column + 1] && slot === player[row + 2][column + 2] && slot === player[row + 3][column + 3]) {
          return currentPlayer;
        }
      }
    }
  }

  for (let row = 0; row < edgeY; row++) {
    for (let column = 0; column < player[0].length; column++) {
      slot = player[row][column];
      
      if (slot !== 0) {
        if (slot === player[row + 1][column - 1] && slot === player[row + 2][column - 2] && slot === player[row + 3][column - 3]) {
          return currentPlayer;
        }
      }
    }
  }
}

const showResult = (result) => {
  resultImage.src = './images/victory.png';

  if (result === 1) {
    resultMessage.style.color = '#ff6b6b';
    resultMessage.textContent = `Congratulations ${player1Name}, you are the winner!`;
  } else if (result === 2) {
    resultMessage.style.color = '#0abde3';
    resultMessage.textContent = `Congratulations ${player2Name}, you are the winner!`;
  } else {
    resultImage.src = './images/draw.png';
    resultMessage.style.color = '#feca57';
    resultMessage.textContent = `There was a draw... Try again!`;
  }

  resultBox.classList.add('show');

  resetButton.addEventListener('click', reset__game);
}

sayWelcome();
closeWelcome();
renderBoard();
startGame();

